<?php

use think\facade\Db;

// 自动载入
require __DIR__ . '/../vendor/autoload.php';

// 数据库配置信息设置（全局有效）
Db::setConfig(require __DIR__ . '/../config/databases.php');

// 路由配置
$SERVER_NAME = $_SERVER['HTTP_HOST'];
preg_match('/(.*\.)?\w+\.\w+$/', $SERVER_NAME, $matches);
if (!empty($matches[1])) {
    $domain_url = trim($matches[1], '.');
} else {
    $domain_url = '';
}
if ($domain_url == 'api') require __DIR__ . '/../route/api.php';
else require __DIR__ . '/../route/app.php';
