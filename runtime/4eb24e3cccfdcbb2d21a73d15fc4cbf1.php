<?php /*a:2:{s:50:"D:\wwwroot\composer\dta\app\views\admin\index.html";i:1592617320;s:50:"D:\wwwroot\composer\dta\app\views\public\full.html";i:1592640120;}*/ ?>
<!DOCTYPE html>
<html lang="zh">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="keywords" content="LightYear,LightYearAdmin,光年,后台模板,后台管理系统,光年HTML模板">
    <meta name="description" content="Light Year Admin V4是一个基于Bootstrap v4.4.1的后台管理系统的HTML模板。">
    <meta name="author" content="yinq">
    <title>数据应用</title>
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <link rel="stylesheet" type="text/css" href="/static/css/materialdesignicons.min.css">
    <link rel="stylesheet" type="text/css" href="/static/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/static/js/bootstrap-multitabs/multitabs.min.css">
    <link rel="stylesheet" type="text/css" href="/static/css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="/static/css/style.min.css">
    <style>
        #logo a {
            font-size: 22px;
            line-height: 68px;
            white-space: nowrap;
            color: #4d5259;
        }

        [data-logobg*='color_'] #logo a {
            color: #fff;
        }

        @media (min-width: 1024px) {
            .lyear-layout-sidebar.lyear-aside-open #logo a {
                display: block;
                width: 45px;
                height: 68px;
                letter-spacing: 3px;
                margin: 0 auto;
                overflow: hidden;
                text-align: center;
            }

            .lyear-layout-sidebar-close .lyear-layout-sidebar:hover #logo a {
                width: 100%;
                margin: 0;
                letter-spacing: 0px;
            }
        }
    </style>
</head>

<body>
<div class="lyear-layout-web">
    <div class="lyear-layout-container">
        <!--左侧导航-->
        <aside class="lyear-layout-sidebar">

            <!-- logo -->
            <div id="logo" class="sidebar-header">
                <a href="<?php echo uri('admin/index'); ?>">数据应用
            </div>
            <div class="lyear-layout-sidebar-info lyear-scroll">

                <nav class="sidebar-main">
                    <ul class="nav-drawer">
                        <li class="nav-item active">
                            <a class="multitabs" href="<?php echo uri('admin/main'); ?>">
                                <i class="mdi mdi-home"></i> <span>后台首页</span>
                            </a>
                        </li>
                        <li class="nav-item nav-item-has-subnav">
                            <a href="javascript:void(0)">
                                <i class="mdi mdi-format-align-justify"></i>
                                <span>Innova</span>
                            </a>
                            <ul class="nav nav-subnav">
                                <li>
                                    <a class="multitabs" href="<?php echo uri('innova/6100p'); ?>">6100P List</a>
                                </li>
                                <li>
                                    <a class="multitabs" href="<?php echo uri('innova/7100p'); ?>">7100P List</a>
                                </li>
                                <li>
                                    <a class="multitabs" href="<?php echo uri('innova/search'); ?>">Search</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>

                <div class="sidebar-footer">
                    <p class="copyright">©版权所有 2017-2020 | 粤ICP备16022000号 | 粤公网安备44090402441014号
                    </p>
                </div>
            </div>

        </aside>
        <!--End 左侧导航-->

        <!--头部信息-->
        <header class="lyear-layout-header">

            <nav class="navbar">

                <div class="navbar-left">
                    <div class="lyear-aside-toggler">
                        <span class="lyear-toggler-bar"></span>
                        <span class="lyear-toggler-bar"></span>
                        <span class="lyear-toggler-bar"></span>
                    </div>
                </div>

                <ul class="navbar-right d-flex align-items-center">

                    <li class="dropdown dropdown-profile">
                        <a href="javascript:void(0)" data-toggle="dropdown" class="dropdown-toggle">
                            <img class="img-avatar img-avatar-48 m-r-10" src="/logo.png"
                                 alt="数据应用"/>
                            <span>数据应用</span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li>
                                <a class="multitabs dropdown-item" data-url="#"
                                   href="javascript:void(0)"><i class="mdi mdi-account"></i> 个人信息</a>
                            </li>
                            <li>
                                <a class="multitabs dropdown-item" data-url="#"
                                   href="javascript:void(0)"><i class="mdi mdi-lock-outline"></i> 修改密码</a>
                            </li>
                            <li>
                                <a class="dropdown-item" href="javascript:void(0)"><i
                                        class="mdi mdi-delete"></i>清空缓存</a>
                            </li>
                            <li class="dropdown-divider"></li>
                            <li>
                                <a class="dropdown-item" data-url="#"
                                <i class="mdi mdi-logout-variant"></i> 退出登录
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>

            </nav>

        </header>
        <!--End 头部信息-->

        <!--页面主要内容-->
        <main class="lyear-layout-content">

            <div id="iframe-content"></div>

        </main>
        <!--End 页面主要内容-->
    </div>
</div>

<script type="text/javascript" src="/static/js/jquery.min.js"></script>
<script type="text/javascript" src="/static/js/popper.min.js"></script>
<script type="text/javascript" src="/static/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/static/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="/static/js/bootstrap-multitabs/multitabs.js"></script>
<script type="text/javascript" src="/static/js/jquery.cookie.min.js"></script>
<script type="text/javascript" src="/static/js/index.min.js"></script>

</body>
</html>
