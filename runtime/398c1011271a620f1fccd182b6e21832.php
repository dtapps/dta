<?php /*a:1:{s:52:"D:\wwwroot\composer\dta\app\views\Innova\search.html";i:1592647993;}*/ ?>
<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
    <title>下拉选择 - 光年(Light Year Admin V4)后台管理系统模板</title>
    <link rel="icon" href="/favicon.ico" type="image/ico">
    <meta name="keywords" content="LightYear,LightYearAdmin,光年,后台模板,后台管理系统,光年HTML模板">
    <meta name="description" content="Light Year Admin V4是一个后台管理系统的HTML模板，基于Bootstrap v4.4.1。">
    <meta name="author" content="yinqi">
    <link href="/static/css/bootstrap.min.css" rel="stylesheet">
    <link href="/static/css/materialdesignicons.min.css" rel="stylesheet">
    <link href="/static/js/bootstrap-select/bootstrap-select.css" rel="stylesheet">
    <link href="/static/css/style.min.css" rel="stylesheet">
</head>

<body>
<div class="container-fluid p-t-15">

    <div class="row">

        <div class="col-lg-12">
            <div class="card">
                <header class="card-header">
                    <div class="card-title">Search</div>
                </header>
                <div class="card-body">

                    <div class="card-toolbar clearfix">
                        <form class="form-inline" action="#!" method="post">
                            <div class="form-group mb-2 mr-2">
                                <label class="input-group-text" for="search_type">Type</label>
                                <div class="form-group">
                                    <select class="form-control selectpicker" name="search_type" id="search_type">
                                        <option value="6100P">6100P</option>
                                        <option value="7100P">7100P</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group mb-2 mr-2" onclick="search_year()">
                                <label class="input-group-text" for="search_year">Year</label>
                                <div class="form-group">
                                    <select class="form-control selectpicker" name="search_year" id="search_year">
                                    </select>
                                </div>
                            </div>
                            <div class="form-group mb-2 mr-2" onclick="search_make()">
                                <label class="input-group-text" for="search_make">Make</label>
                                <div class="form-group">
                                    <select class="form-control selectpicker" name="search_make" id="search_make">
                                    </select>
                                </div>
                            </div>
                            <div class="form-group mb-2 mr-2" onclick="search_model()">
                                <label class="input-group-text" for="search_model">Model</label>
                                <div class="form-group">
                                    <select class="form-control selectpicker" name="search_model" id="search_model">
                                    </select>
                                </div>
                            </div>
                            <a class="btn btn-default mb-2 mr-2" href="#!" onclick="get()">
                                <i class="mdi mdi-magnify"></i>Search
                            </a>
                        </form>
                    </div>

                </div>

                <div class="lyear-divider" style="display: none" id="rsp_result">result</div>

                <div class="table-responsive" style="display: none" id="rsp_info">
                    <table class="table">
                        <thead>
                        <tr>
                            <th id="check_engine_name">Check Engine</th>
                            <th id="abs_name">Abs</th>
                            <th id="srs_name">Srs</th>
                            <th id="oil_reset_proceoure_name" style="display: none">Oil Reset Proceoure</th>
                            <th id="battery_proceoure_name">Battery Proceoure</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td id="check_engine_val"></td>
                            <td id="abs_val"></td>
                            <td id="srs_val"></td>
                            <td id="oil_reset_proceoure_val" style="display: none"></td>
                            <td id="battery_proceoure_val"></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

</div>
<script type="text/javascript" src="/static/js/jquery.min.js"></script>
<script type="text/javascript" src="/static/js/popper.min.js"></script>
<script type="text/javascript" src="/static/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/static/js/bootstrap-select/bootstrap-select.min.js"></script>
<script type="text/javascript" src="/static/js/bootstrap-select/i18n/defaults-zh_CN.min.js"></script>
<script type="text/javascript" src="/static/js/main.min.js"></script>
<script type="text/javascript" src="/static/js/layer/layer.js"></script>
<script type="text/javascript">
    function get() {

        document.getElementById("rsp_result").style.display = "none";
        document.getElementById("rsp_info").style.display = "none";
        document.getElementById("oil_reset_proceoure_name").style.display = "none";
        document.getElementById("oil_reset_proceoure_val").style.display = "none";

        var search_type = $("#search_type option:selected");
        var search_year = $("#search_year option:selected");
        var search_make = $("#search_make option:selected");
        var search_model = $("#search_model option:selected");
        if (judgeK(search_type.val()) === false && judgeK(search_year.val()) === false && judgeK(search_make.val()) === false && judgeK(search_model.val()) === false) {
            $.get("<?php echo uri('innova/getsSelectRsp'); ?>", {
                type: search_type.val(),
                year: search_year.val(),
                make: search_make.val(),
                model: search_model.val(),
            }, function (res) {
                if (res.data !== []) {
                    console.log(res.data)
                    document.getElementById("check_engine_val").innerHTML = res.data.check_engine;
                    document.getElementById("abs_val").innerHTML = res.data.abs;
                    document.getElementById("srs_val").innerHTML = res.data.srs;
                    document.getElementById("battery_proceoure_val").innerHTML = res.data.battery_proceoure;
                    if (search_type.val() == "7100P") {
                        document.getElementById("oil_reset_proceoure_val").innerHTML = res.data.oil_reset_proceoure;
                        document.getElementById("oil_reset_proceoure_name").style.display = "block";
                        document.getElementById("oil_reset_proceoure_val").style.display = "block";
                    }
                    document.getElementById("rsp_result").style.display = "block";
                    document.getElementById("rsp_info").style.display = "block";
                } else {
                    layer.msg("暂无结果")
                }
            });
        } else {
            layer.msg("Please check selection")
        }
    }

    function search_year() {
        var search_type = $("#search_type option:selected");
        if (judgeK(search_type.val()) === false) {
            $.get("<?php echo uri('innova/getsSelect/type/year'); ?>", {
                type: search_type.val()
            }, function (res) {
                var data = res.data;
                for (var i = 0; i < data.length; i++) {
                    $("#search_year").append("<option value='" + data[i].name + "'>" + data[i].name + "</option>");
                }
                //使用refresh方法更新UI以匹配新状态。
                $('#search_year').selectpicker('refresh');
                //render方法强制重新渲染引导程序 - 选择ui。
                $('#search_year').selectpicker('render');
            });
            $('#search_make').selectpicker('val', ['noneSelectedText'])//回到初始状态
            $("#search_make").selectpicker('refresh');//刷新
            $('#search_model').selectpicker('val', ['noneSelectedText'])//回到初始状态
            $("#search_model").selectpicker('refresh');//刷新
            search_make()
        }
    }

    function search_make() {
        var search_type = $("#search_type option:selected");
        var search_year = $("#search_year option:selected");
        if (judgeK(search_type.val()) === false && judgeK(search_year.val()) === false) {
            $.get("<?php echo uri('innova/getsSelect/type/make'); ?>", {
                type: search_type.val(),
                year: search_year.val(),
            }, function (res) {
                var data = res.data;
                for (var i = 0; i < data.length; i++) {
                    $("#search_make").append("<option value='" + data[i].name + "'>" + data[i].name + "</option>");
                }
                //使用refresh方法更新UI以匹配新状态。
                $('#search_make').selectpicker('refresh');
                //render方法强制重新渲染引导程序 - 选择ui。
                $('#search_make').selectpicker('render');
            });
            $('#search_model').selectpicker('val', ['noneSelectedText'])//回到初始状态
            $("#search_model").selectpicker('refresh');//刷新
            search_model()
        }
    }

    function search_model() {
        var search_type = $("#search_type option:selected");
        var search_year = $("#search_year option:selected");
        var search_make = $("#search_make option:selected");
        if (judgeK(search_type.val()) === false && judgeK(search_year.val()) === false && judgeK(search_make.val()) === false) {
            $.get("<?php echo uri('innova/getsSelect/type/model'); ?>", {
                type: search_type.val(),
                year: search_year.val(),
                make: search_make.val(),
            }, function (res) {
                var data = res.data;
                for (var i = 0; i < data.length; i++) {
                    $("#search_model").append("<option value='" + data[i].name + "'>" + data[i].name + "</option>");
                }
                //使用refresh方法更新UI以匹配新状态。
                $('#search_model').selectpicker('refresh');
                //render方法强制重新渲染引导程序 - 选择ui。
                $('#search_model').selectpicker('render');
            });
        }
    }

    function judgeK(a) {
        if (a === undefined) { // 只能用 === 运算来测试某个值是否是未定义的
            return true;
        }
        if (a == null) { // 等同于 a === undefined || a === null
            return true;
        }

        // String
        if (a == "" || a == null || a == undefined) { // "",null,undefined
            return true;
        }
        if (!a) { // "",null,undefined,NaN
            return true;
        }
        if (!$.trim(a)) { // "",null,undefined
            return true;
        }

        // Array
        if (a.length == 0) { // "",[]
            return true;
        }
        if (!a.length) { // "",[]
            return true;
        }

        // Object {}
        if ($.isEmptyObject(a)) { // 普通对象使用 for...in 判断，有 key 即为 false
            return true;
        }
        return false;

    }
</script>
</body>
</html>
