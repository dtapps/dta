<?php

use \NoahBuscher\Macaw\Macaw;

Macaw::get('/', 'app\www\controller\Index@index');


//Macaw::get('/', function () {
//    echo 'Hello World!';
//});

//Macaw::get('btc/index', 'app\controllers\Btc@index');
//Macaw::get('btc/dr', 'app\controllers\Btc@dr');
//Macaw::get('btc/bf', 'app\controllers\Btc@bf');

Macaw::get('admin/index', function () {
    view('admin/index');
});
Macaw::get('admin/main', function () {
    view('admin/main');
});

Macaw::get('innova/6100p', function () {
    view('Innova/6100p', [
        'title' => "6100p"
    ]);
});
Macaw::get('innova/7100p', function () {
    view('Innova/7100p', [
        'title' => "7100P"
    ]);
});
Macaw::get('innova/search', function () {
    view('Innova/search', [
        'title' => "7100P"
    ]);
});
Macaw::get('v1/innova/getList/type/(:any)', 'app\controllers\v1\Innova@getList');

Macaw::get('v1/innova/getsSelect/type/(:any)', 'app\controllers\v1\Innova@getsSelect');
Macaw::get('v1/innova/getsSelectRsp', 'app\controllers\v1\Innova@getsSelectRsp');

/**
 * Ip
 */
Macaw::get('v1/ip/qqwry', 'app\controllers\v1\Ip@qqwry');

Macaw::dispatch();
