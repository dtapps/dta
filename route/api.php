<?php

use \NoahBuscher\Macaw\Macaw;

Macaw::get('/', function () {
    echo 'Hello World!';
});


Macaw::get('v1/innova/getList/type/(:any)', 'app\controllers\v1\Innova@getList');

Macaw::get('v1/innova/getsSelect/type/(:any)', 'app\controllers\v1\Innova@getsSelect');
Macaw::get('v1/innova/getsSelectRsp', 'app\controllers\v1\Innova@getsSelectRsp');

/**
 * Ip
 */
Macaw::get('v1/ip/qqwry', 'app\api\controller\v1\Ip@qqwry');

Macaw::dispatch();
