<?php
// 模板引擎配置
return [
    // 模板目录名
    'view_path' => dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'view' . DIRECTORY_SEPARATOR,
    // 模板缓存
    'cache_path' => dirname(__DIR__) . DIRECTORY_SEPARATOR . 'runtime' . DIRECTORY_SEPARATOR,
    // 模板后缀
    'view_suffix' => 'html',
    // 是否开启模板编译缓存,设为false则每次都会重新编译
    'tpl_cache' => false,
    // 	模板缓存有效期 0 为永久，(以数字为值，单位:秒)
    'cache_time' => 1
];
