<?php

namespace app\api\controller\v1;

use DtApp\QqWry\Client;
use DtApp\QqWry\QqWryException;

class Ip
{
    public function qqwry()
    {
        $client = new Client();
        try {
            return success($client->getLocation());
        } catch (QqWryException $e) {
            return error($e->getMessage());
        }
    }
}
