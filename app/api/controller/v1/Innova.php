<?php

namespace app\api\controller\v1;

use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;
use think\facade\Db;

class Innova
{
    private $table_yx_6100p = 'yx_6100p';
    private $table_yx_7100p = 'yx_7100p';

    /**
     * 获取列表数据
     * @param string $type
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function getList($type = '6100p')
    {
        $page = input('get.page', 1);
        $limit = input('get.limit', 10);
        $year = input('get.year', '');
        $make = input('get.make', '');
        $model = input('get.model', '');
        $where = [];
        if (!empty($year)) $where['year'] = $year;
        if (!empty($make)) $where['make'] = $make;
        if (!empty($model)) $where['model'] = $model;
        if ($type == '6100p') {
            $data = Db::table($this->table_yx_6100p)
                ->page($page)
                ->limit($limit)
                ->where($where)
                ->select()
                ->toArray();
            $total = Db::table($this->table_yx_6100p)
                ->where($where)
                ->count();
        } elseif ($type == '7100p') {
            $data = Db::table($this->table_yx_7100p)
                ->page($page)
                ->limit($limit)
                ->where($where)
                ->select()
                ->toArray();
            $total = Db::table($this->table_yx_7100p)
                ->where($where)
                ->count();
        } else {
            $data = [];
            $total = 0;
        }
        return success([
            'rows' => $data,
            'total' => $total,
        ]);
    }

    /**
     * 获取选择列表数据
     * @param string $type
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function getsSelect($type = 'year')
    {
        $cal = input('get.type', '6100P');
        $year = input('get.year', '');
        $make = input('get.make', '');
        $model = input('get.model', '');
        $data = [];
        $where = [];
        if (!empty($year)) $where['year'] = $year;
        if (!empty($make)) $where['make'] = $make;
        if (!empty($model)) $where['model'] = $model;
        if ($cal == '6100P') {
            $data = Db::table($this->table_yx_6100p)
                ->group($type)
                ->field("{$type} as name")
                ->where($where)
                ->select()
                ->toArray();
        } elseif ($cal == '7100P') {
            $data = Db::table($this->table_yx_7100p)
                ->group($type)
                ->field("{$type} as name")
                ->where($where)
                ->select()
                ->toArray();
        }
        return success($data);
    }

    public function getsSelectRsp()
    {
        $type = input('get.type', '6100P');
        $year = input('get.year', '');
        $make = input('get.make', '');
        $model = input('get.model', '');
        $where = [];
        $data = [];
        if (!empty($year)) $where['year'] = $year;
        if (!empty($make)) $where['make'] = $make;
        if (!empty($model)) $where['model'] = $model;
        if ($type == '6100P') {
            $data = Db::table($this->table_yx_6100p)
                ->where($where)
                ->field("check_engine,abs,srs,battery_proceoure")
                ->find();
        } elseif ($type == '7100P') {
            $data = Db::table($this->table_yx_7100p)
                ->where($where)
                ->field("check_engine,abs,srs,oil_reset_proceoure,battery_proceoure")
                ->find();
        }
        if (empty($data)) $data = [];
        return success($data);
    }
}
