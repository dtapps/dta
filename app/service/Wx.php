<?php


use DtApp\Curl\Get;
use DtApp\Curl\Post;
use think\facade\Db;

require '../../vendor/autoload.php';


function notice()
{
    Db::setConfig([
        // 默认数据连接标识
        'default' => 'mysql',
        // 数据库连接信息
        'connections' => [
            'mysql' => [
                // 数据库类型
                'type' => 'mysql',
                // 主机地址
                'hostname' => '139.159.219.224',
                // 用户名
                'username' => 'dtapp',
                // 数据库名
                'database' => 'dtapp',
                // 数据库密码
                'password' => 'rh8AALkEpzklpOGW',
                // 数据库连接端口
                'hostport' => '3301',
                // 数据库编码默认采用utf8
                'charset' => 'utf8mb4',
                // 数据库表前缀
                'prefix' => '',
                // 数据库调试模式
                'debug' => true,
            ],
        ],
    ]);

    $token = Db::table('think_cache')
        ->where('cache_name', "wx6f53456f42d5d1c9_access_token")
        ->order('id desc')
        ->whereTime('cache_expire', '>', time())
        ->value('cache_value');
    if (empty($token)) {
        $get = new Get();
        $res = $get->http("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wx6f53456f42d5d1c9&secret=c3f4c3d03ab480c4bf17dc528ed97d0c",
            '', true
        );
        Db::table('think_cache')
            ->insert([
                'cache_name' => "wx6f53456f42d5d1c9_access_token",
                'cache_value' => $res['access_token'],
                'cache_expire' => date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s", time())) - 6000)
            ]);
        $token = $res['access_token'];
    }
    // 链接
    $post = new Post();
    $msg = $post->http("https://api.weixin.qq.com/cgi-bin/message/template/send?access_token={$token}", [
        "touser" => "ohWG3s0nB13WYDP20eJcyC-tY6Mo",
//        "touser" => "ohWG3s6ywGHvzsGGaOZva-9WcquI",
        "template_id" => "VTKj33jtnW9QddTFKHalyq7hFLJ0fbiq-aoXzS4pJsU",
        "data" => [
            "first" => [
                "value" => "BG大单提醒",
                "color" => "#173177"
            ],
            // 名称
            "keyword1" => [
                "value" => "BTC",
                "color" => "#173177"
            ],
            // 价格
            "keyword2" => [
                "value" => "400000",
                "color" => "#173177"
            ],
            // 方向
            "keyword3" => [
                "value" => '开仓',
                "color" => "#173177"
            ],
            // 数量
            "keyword4" => [
                "value" => "20",
                "color" => "#173177"
            ],
            // 时间
            "keyword5" => [
                "value" => "2020年6月19日15:33:34"],
            "color" => "#173177"
        ],
        "remark" => [
            "value" => "推送成功",
            "color" => "#173177"
        ]
    ], true);
    return $msg;
}
