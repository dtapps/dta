<?php
// 应用公共文件

use think\Template;

if (!function_exists('view')) {
    /**
     * 渲染模板
     * @param string $temp
     * @param array $var
     */
    function view($temp, $var = [])
    {
        // 设置模板引擎参数
        $config = require __DIR__ . '/../config/view.php';
        $template = new Template();
        dump($config);
        $template->config($config);
        $template->fetch($temp, $var);
    }
}

if (!function_exists('uri')) {
    /**
     * 返回Uri
     * @param $url
     * @return string
     */
    function uri($url)
    {
        return "//" . $_SERVER['SERVER_NAME'] . "/{$url}";
    }
}

if (!function_exists('response')) {
    /**
     * 返回 - Json
     * @param array $data
     */
    function response(array $data = [])
    {
        date_default_timezone_set('Asia/Shanghai');
        header('Content-Type:application/json; charset=utf-8');
        echo json_encode($data, JSON_UNESCAPED_UNICODE);
        exit();
    }
}

if (!function_exists('success')) {
    /**
     * 返回成功 - Json
     * @param array $data
     * @param string $msg
     * @param int $code
     * @param array $ext
     */
    function success(array $data = [], string $msg = 'success', int $code = 0, array $ext = [])
    {
        date_default_timezone_set('Asia/Shanghai');
        header('Content-Type:application/json; charset=utf-8');
        if (!empty($ext) && is_array($ext)) {
            echo json_encode(array_merge(['code' => $code, 'msg' => $msg, 'time' => time(), 'data' => $data], $ext), JSON_UNESCAPED_UNICODE);
            exit();
        } else {
            echo json_encode(['code' => $code, 'msg' => $msg, 'time' => time(), 'data' => $data], JSON_UNESCAPED_UNICODE);
            exit();
        }
    }
}

if (!function_exists('error')) {
    /**
     * 返回失败 - Json
     * @param string $msg
     * @param int $code
     * @param array $data
     * @param array $ext
     */
    function error(string $msg = 'error', int $code = 1, array $data = [], array $ext = [])
    {
        date_default_timezone_set('Asia/Shanghai');
        header('Content-Type:application/json; charset=utf-8');
        if (!empty($ext) && is_array($ext)) {
            echo json_encode(array_merge(['code' => $code, 'msg' => $msg, 'time' => time(), 'data' => $data], $ext), JSON_UNESCAPED_UNICODE);
            exit();
        } else {
            echo json_encode(['code' => $code, 'msg' => $msg, 'time' => time(), 'data' => $data], JSON_UNESCAPED_UNICODE);
            exit();
        }
    }
}


if (!function_exists('input')) {
    /**
     * 输入
     * @param string $name
     * @param $default
     * @return mixed|string
     */
    function input(string $name, $default)
    {
        if (strpos($name, '.') !== false) {
            $type = substr($name, 0, strpos($name, '.'));
            $name = substr($name, strripos($name, ".") + 1);
            if ($type == 'get') {
                $value = $_GET["$name"];
                if (empty($value)) return $default;
                return $value;
            }
            if ($type == 'post') {
                $value = $_POST["$name"];
                if (empty($value)) return $default;
                return $value;
            }
            return '';
        } else {
            return '';
        }
    }
}


